set(COMPONENT_ADD_INCLUDEDIRS ".")

set(COMPONENT_SRCS "ArduinoBluepad32.cpp"
                   "ArduinoGamepad.cpp"
                   )

set(COMPONENT_REQUIRES "bluepad32")

register_component()
